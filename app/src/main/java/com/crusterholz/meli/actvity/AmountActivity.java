package com.crusterholz.meli.actvity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.crusterholz.meli.R;
import com.crusterholz.meli.actvity.base.BaseActivity;
import com.crusterholz.meli.model.Amount;
import com.crusterholz.meli.model.CardIssue;
import com.crusterholz.meli.model.PayerCost;
import com.crusterholz.meli.model.PaymentMethod;
import com.crusterholz.meli.utils.FormatUtils;

public class AmountActivity extends BaseActivity {

    EditText amountEditText;
    Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);

        initActionBar(getString(R.string.title_activity_amount),false);
        bindComponents();
        verifyResults();
    }

    public void bindComponents(){
        amountEditText = findViewById(R.id.amountEditText);
        amountEditText.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    if(s.toString().indexOf(FormatUtils.getCurrencySymbol()) == -1){
                        amountEditText.removeTextChangedListener(this);
                        current = FormatUtils.getCurrencySymbol() + s;
                        amountEditText.setText(current);
                        amountEditText.setSelection(current.length());
                        amountEditText.addTextChangedListener(this);
                    }else if (FormatUtils.getCurrencySymbol().equals(s.toString())){
                        current = "";
                        amountEditText.setText(current);
                        amountEditText.setSelection(current.length());
                        amountEditText.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FormatUtils.isValidAmountInput(amountEditText.getText().toString())){
                    nextButton.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    navigateToPaymentMethodActivity();
                }
            }
        });
    }

    public void verifyResults(){
        Amount amount = (Amount) getIntent().getSerializableExtra("amount");
        PaymentMethod paymentMethod = (PaymentMethod) getIntent().getSerializableExtra("paymentMethod");
        CardIssue cardIssue = (CardIssue) getIntent().getSerializableExtra("cardIssue");
        PayerCost payerCost = (PayerCost) getIntent().getSerializableExtra("payerCost");

        if(amount != null && paymentMethod != null && cardIssue != null && payerCost != null) {
            showInformationDialogResults(amount, paymentMethod, cardIssue, payerCost);
        }
    }



    public void navigateToPaymentMethodActivity(){
        Intent nextIntent = new Intent(AmountActivity.this, PaymentMethodActivity.class);
        Amount amount = new Amount(amountEditText.getText().toString());
        nextIntent.putExtra("amount", amount);
        startActivity(nextIntent);
    }

    public void showInformationDialogResults(Amount amount, PaymentMethod paymentMethod, CardIssue cardIssue, PayerCost payerCost){
        String message =
                "Monto: " + "\n" + "\t" + FormatUtils.formatAmount(amount.getAmount()) + "\n\n" +
                "Tarjeta: " + "\n" + "\t" + paymentMethod.getName() + "\n\n" +
                "Banco: " + "\n" + "\t" + cardIssue.getName() + "\n\n" +
                "Cuotas: " + "\n" + "\t" + payerCost.getRecommendedMessage();

        showInformationDialog(getString(R.string.payment_confirmation_title), message);
    }
}
