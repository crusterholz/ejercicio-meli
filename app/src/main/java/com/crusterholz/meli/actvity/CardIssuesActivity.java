package com.crusterholz.meli.actvity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crusterholz.meli.R;
import com.crusterholz.meli.actvity.base.BaseActivity;
import com.crusterholz.meli.adapter.CardIssueAdapter;
import com.crusterholz.meli.adapter.OnCardIssueSelectedListener;
import com.crusterholz.meli.model.Amount;
import com.crusterholz.meli.model.CardIssue;
import com.crusterholz.meli.model.PaymentMethod;
import com.crusterholz.meli.service.getCardIssues.GetCardIssuesImpl;
import com.crusterholz.meli.service.getCardIssues.model.GetCardIssuesRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardIssuesActivity extends BaseActivity implements OnCardIssueSelectedListener {

    private RecyclerView recyclerView;
    private CardIssueAdapter adapter;
    private Amount amount;
    private PaymentMethod paymentMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_issues);

        initActionBar(true);
        bindComponents();

        getCardIssues();
    }

    public void bindComponents(){
        adapter = new CardIssueAdapter(this);
        recyclerView = findViewById(R.id.recyclerViewCardIssue);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    public void getCardIssues(){
        amount = (Amount) getIntent().getSerializableExtra("amount");
        paymentMethod = (PaymentMethod) getIntent().getSerializableExtra("paymentMethod");

        showLoading();
        GetCardIssuesRequest getCardIssuesRequest = new GetCardIssuesRequest();
        getCardIssuesRequest.setPaymentMethodId(paymentMethod.getId());
        GetCardIssuesImpl getCardIssues = new GetCardIssuesImpl();
        getCardIssues.get(getCardIssuesRequest, new Callback<List<CardIssue>>() {
            @Override
            public void onResponse(Call<List<CardIssue>> call, Response<List<CardIssue>> response) {
                showCardIssues(response.body());
            }

            @Override
            public void onFailure(Call<List<CardIssue>> call, Throwable t) {
                hideLoading();
                showInformationDialog(getString(R.string.error_ws));
            }
        });
    }

    public void showCardIssues(List<CardIssue> cardIssues){
        if(cardIssues != null && cardIssues.size() > 0) {
            adapter.updateData(cardIssues);
        }else{
            showInformationDialog(getString(R.string.message_no_card_issuer));
        }
        hideLoading();
    }

    public void navigateToInstallmentsActivity(CardIssue cardIssue){
        Intent nextIntent = new Intent(CardIssuesActivity.this, InstallmentsActivity.class);
        nextIntent.putExtra("amount", amount);
        nextIntent.putExtra("paymentMethod", paymentMethod);
        nextIntent.putExtra("cardIssue", cardIssue);
        startActivity(nextIntent);
    }

    @Override
    public void onCardIssueSelectedListener(CardIssue cardIssue) {
        navigateToInstallmentsActivity(cardIssue);
    }
}
