package com.crusterholz.meli.actvity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crusterholz.meli.R;
import com.crusterholz.meli.actvity.base.BaseActivity;
import com.crusterholz.meli.adapter.InstallmentsAdapter;
import com.crusterholz.meli.adapter.OnInstallmentSelectedListener;
import com.crusterholz.meli.model.Amount;
import com.crusterholz.meli.model.CardIssue;
import com.crusterholz.meli.model.Installment;
import com.crusterholz.meli.model.PayerCost;
import com.crusterholz.meli.model.PaymentMethod;
import com.crusterholz.meli.service.getInstallments.GetInstallmentsImpl;
import com.crusterholz.meli.service.getInstallments.model.GetInstallmentsRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstallmentsActivity extends BaseActivity implements OnInstallmentSelectedListener {

    RecyclerView recyclerView;
    InstallmentsAdapter adapter;
    Amount amount;
    PaymentMethod paymentMethod;
    CardIssue cardIssue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installments);

        initActionBar(true);
        bindComponents();

        getInstallments();
    }

    public void bindComponents(){
        adapter = new InstallmentsAdapter(this);
        recyclerView = findViewById(R.id.recyclerViewInstallment);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    public void getInstallments() {
        amount = (Amount) getIntent().getSerializableExtra("amount");
        paymentMethod = (PaymentMethod) getIntent().getSerializableExtra("paymentMethod");
        cardIssue = (CardIssue) getIntent().getSerializableExtra("cardIssue");

        showLoading();
        GetInstallmentsRequest getInstallmentsRequest = new GetInstallmentsRequest();
        getInstallmentsRequest.setAmount(amount.getAmount());
        getInstallmentsRequest.setIssuerId(cardIssue.getId());
        getInstallmentsRequest.setPaymentMethodId(paymentMethod.getId());

        GetInstallmentsImpl getInstallments = new GetInstallmentsImpl();
        getInstallments.get(getInstallmentsRequest, new Callback<List<Installment>>() {
            @Override
            public void onResponse(Call<List<Installment>> call, Response<List<Installment>> response) {
                showInstallments(response.body());
            }

            @Override
            public void onFailure(Call<List<Installment>> call, Throwable t) {
                hideLoading();
                showInformationDialog(getString(R.string.error_ws));
            }
        });
    }

    public void showInstallments(List<Installment> installments){
        if(installments != null && installments.size() > 0 && installments.get(0).getPayerCosts().size() > 0) {
            adapter.updateData(installments);
        }else{
            showInformationDialog(getString(R.string.message_no_intallments));
        }
        hideLoading();
    }

    public void navigateToAmountActivity(PayerCost payerCost){
        Intent nextIntent = new Intent(InstallmentsActivity.this, AmountActivity.class);
        nextIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        nextIntent.putExtra("amount", amount);
        nextIntent.putExtra("paymentMethod", paymentMethod);
        nextIntent.putExtra("cardIssue", cardIssue);
        nextIntent.putExtra("payerCost", payerCost);
        startActivity(nextIntent);
    }

    @Override
    public void onCardIssueSelectedListener(PayerCost payerCost) {
        navigateToAmountActivity(payerCost);
    }
}
