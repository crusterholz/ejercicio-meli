package com.crusterholz.meli.actvity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crusterholz.meli.R;
import com.crusterholz.meli.actvity.base.BaseActivity;
import com.crusterholz.meli.adapter.OnPaymentMethodSelectedListener;
import com.crusterholz.meli.adapter.PaymentMethodAdapter;
import com.crusterholz.meli.model.Amount;
import com.crusterholz.meli.model.PaymentMethod;
import com.crusterholz.meli.service.getPaymentMethods.GetPaymentMethodsImpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodActivity extends BaseActivity implements OnPaymentMethodSelectedListener {

    RecyclerView recyclerView;
    private PaymentMethodAdapter adapter;
    private Amount amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        initActionBar(true);
        bindComponents();

        getPaymentMethods();
    }

    public void bindComponents(){
        adapter = new PaymentMethodAdapter(this);
        recyclerView = findViewById(R.id.recyclerViewPaymentMethod);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    public void getPaymentMethods(){
        amount = (Amount) getIntent().getSerializableExtra("amount");
        showLoading();
        GetPaymentMethodsImpl getPaymentMethods = new GetPaymentMethodsImpl();
        getPaymentMethods.get(new Callback<List<PaymentMethod>>() {
            @Override
            public void onResponse(Call<List<PaymentMethod>> call, Response<List<PaymentMethod>> response) {
                showPaymentMethods(response.body());
            }

            @Override
            public void onFailure(Call<List<PaymentMethod>> call, Throwable t) {
                hideLoading();
                showInformationDialog(getString(R.string.error_ws));
            }
        });
    }

    public void showPaymentMethods(List<PaymentMethod> paymentMethods){
        List<PaymentMethod> onlyCardsPaymentMethods = new ArrayList<>();

        for (PaymentMethod paymentMethod: paymentMethods) {
            if(paymentMethod.getPaymentType().equals("credit_card"))
                onlyCardsPaymentMethods.add(paymentMethod);
        }

        if(onlyCardsPaymentMethods.size() > 0) {
            adapter.updateData(onlyCardsPaymentMethods);
        }else{
            showInformationDialog(getString(R.string.message_no_payment_methods));
        }
        hideLoading();
    }

    public void navigateToCardIssuesActivity(PaymentMethod paymentMethod){
        Intent nextIntent = new Intent(PaymentMethodActivity.this, CardIssuesActivity.class);
        nextIntent.putExtra("amount", amount);
        nextIntent.putExtra("paymentMethod", paymentMethod);
        startActivity(nextIntent);
    }

    @Override
    public void onPaymentMethodSelected(PaymentMethod paymentMethod) {
        navigateToCardIssuesActivity(paymentMethod);
    }
}
