package com.crusterholz.meli.actvity.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.crusterholz.meli.R;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initProgressDialog();
    }

    protected void initActionBar(String title, Boolean enableBackButton) {
        setTitle(title);
        initActionBar(enableBackButton);
    }

    protected void initActionBar(Boolean enableBackButton) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(enableBackButton);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.message_loading));
        progressDialog.setCancelable(false);
    }

    public void showLoading(){
        progressDialog.show();
    }

    public void hideLoading(){
        progressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }

    public void showInformationDialog(String message){
        showInformationDialog(getString(R.string.dialog_default_title), message);
    }

    public void showInformationDialog(String title, String message){
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
}
