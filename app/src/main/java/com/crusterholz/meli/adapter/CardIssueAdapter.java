package com.crusterholz.meli.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crusterholz.meli.R;
import com.crusterholz.meli.model.CardIssue;
import com.crusterholz.meli.model.PaymentMethod;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CardIssueAdapter extends RecyclerView.Adapter<CardIssueAdapter.ViewHolder> {

    private static List<CardIssue> dataSet;
    private static OnCardIssueSelectedListener onCardIssueSelectedListener;

    public CardIssueAdapter(Context context){
        this.dataSet = new ArrayList<>();
        this.onCardIssueSelectedListener = (OnCardIssueSelectedListener) context;
    }

    public void updateData(List<CardIssue>  data){
        dataSet = data;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CardIssueAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_issue_row_item, viewGroup, false);

        CardIssueAdapter.ViewHolder vh = new CardIssueAdapter.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull CardIssueAdapter.ViewHolder viewHolder, int i) {
        CardIssue cardIssue = dataSet.get(i);
        viewHolder.cardIssueName.setText(cardIssue.getName());
        Picasso.get().load(cardIssue.getSecureThumbnail()).into(viewHolder.cardIssueImage);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView cardIssueName;
        ImageView cardIssueImage;

        public ViewHolder(final View itemView) {
            super(itemView);
            cardIssueName = itemView.findViewById(R.id.card_issue_name);
            cardIssueImage = itemView.findViewById(R.id.card_issue_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CardIssue cardIssue = dataSet.get(getAdapterPosition());
                    onCardIssueSelectedListener.onCardIssueSelectedListener(cardIssue);

                }
            });
        }
    }

}
