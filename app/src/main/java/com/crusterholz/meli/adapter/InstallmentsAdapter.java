package com.crusterholz.meli.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crusterholz.meli.R;
import com.crusterholz.meli.model.Installment;
import com.crusterholz.meli.model.PayerCost;

import java.util.ArrayList;
import java.util.List;

public class InstallmentsAdapter extends RecyclerView.Adapter<InstallmentsAdapter.ViewHolder> {

    private static List<PayerCost> dataSet;
    private static OnInstallmentSelectedListener onInstallmentSelectedListener;

    public InstallmentsAdapter(Context context){
        this.dataSet = new ArrayList<>();
        this.onInstallmentSelectedListener = (OnInstallmentSelectedListener) context;
    }

    public void updateData(List<Installment> data){
        dataSet = data.get(0).getPayerCosts();
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InstallmentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.installment_row_item, viewGroup, false);

        InstallmentsAdapter.ViewHolder vh = new InstallmentsAdapter.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull InstallmentsAdapter.ViewHolder viewHolder, int i) {
        PayerCost payerCost = dataSet.get(i);
        viewHolder.payerCost.setText(payerCost.getRecommendedMessage());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView payerCost;

        public ViewHolder(final View itemView) {
            super(itemView);
            payerCost = itemView.findViewById(R.id.payer_cost);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PayerCost payerCost = dataSet.get(getAdapterPosition());
                    onInstallmentSelectedListener.onCardIssueSelectedListener(payerCost);

                }
            });
        }
    }

}
