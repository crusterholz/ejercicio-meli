package com.crusterholz.meli.adapter;

import com.crusterholz.meli.model.CardIssue;

public interface OnCardIssueSelectedListener {
    void onCardIssueSelectedListener(CardIssue cardIssue);
}