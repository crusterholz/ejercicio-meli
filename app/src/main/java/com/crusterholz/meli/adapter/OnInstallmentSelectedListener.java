package com.crusterholz.meli.adapter;

import com.crusterholz.meli.model.PayerCost;

public interface OnInstallmentSelectedListener {
    void onCardIssueSelectedListener(PayerCost payerCost);
}