package com.crusterholz.meli.adapter;

import com.crusterholz.meli.model.PaymentMethod;

public interface OnPaymentMethodSelectedListener {
    void onPaymentMethodSelected(PaymentMethod paymentMethod);
}