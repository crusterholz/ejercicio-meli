package com.crusterholz.meli.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crusterholz.meli.R;
import com.crusterholz.meli.model.PaymentMethod;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {

    private static List<PaymentMethod> dataSet;
    private static OnPaymentMethodSelectedListener onPaymentMethodSelectedListener;

    public PaymentMethodAdapter(Context context){
        this.dataSet = new ArrayList<>();
        this.onPaymentMethodSelectedListener = (OnPaymentMethodSelectedListener) context;
    }

    public void updateData(List<PaymentMethod> data){
        dataSet = data;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PaymentMethodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.payment_method_row_item, viewGroup, false);

        PaymentMethodAdapter.ViewHolder vh = new PaymentMethodAdapter.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodAdapter.ViewHolder viewHolder, int i) {
        PaymentMethod paymentMethod = dataSet.get(i);
        viewHolder.paymentMethodName.setText(paymentMethod.getName());
        Picasso.get().load(paymentMethod.getSecureThumbnail()).into(viewHolder.paymentMethodImage);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView paymentMethodName;
        ImageView paymentMethodImage;

        public ViewHolder(final View itemView) {
            super(itemView);
            paymentMethodName = itemView.findViewById(R.id.payment_method_name);
            paymentMethodImage = itemView.findViewById(R.id.payment_method_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PaymentMethod paymentMethod = dataSet.get(getAdapterPosition());
                    onPaymentMethodSelectedListener.onPaymentMethodSelected(paymentMethod);

                }
            });
        }
    }

}
