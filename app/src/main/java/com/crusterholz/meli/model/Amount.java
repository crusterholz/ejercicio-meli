package com.crusterholz.meli.model;

import java.io.Serializable;

public class Amount implements Serializable {
    String amount;

    public Amount(String amount){
        this.amount = amount.replaceAll("[$]", "");
    }
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
