package com.crusterholz.meli.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CardIssue implements Serializable {
    String id;
    String name;

    @SerializedName("secure_thumbnail")
    String secureThumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }
}
