package com.crusterholz.meli.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Installment {
    CardIssue issuer;

    @SerializedName("payer_costs")
    List<PayerCost> payerCosts;

    public CardIssue getIssuer() {
        return issuer;
    }

    public void setIssuer(CardIssue issuer) {
        this.issuer = issuer;
    }

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }
}
