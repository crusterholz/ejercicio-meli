package com.crusterholz.meli.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PayerCost implements Serializable {
    @SerializedName("recommended_message")
    String recommendedMessage;

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }
}
