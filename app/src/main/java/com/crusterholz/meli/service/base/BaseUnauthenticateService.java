package com.crusterholz.meli.service.base;

import com.crusterholz.meli.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseUnauthenticateService {
    protected Retrofit retrofit;
    private final int TIMEOUT_UNAUTHENTICATE = 10;

    public BaseUnauthenticateService() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().readTimeout(TIMEOUT_UNAUTHENTICATE, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_UNAUTHENTICATE, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }
}