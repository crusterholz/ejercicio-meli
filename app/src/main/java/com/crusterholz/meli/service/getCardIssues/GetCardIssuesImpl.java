package com.crusterholz.meli.service.getCardIssues;

import com.crusterholz.meli.model.CardIssue;
import com.crusterholz.meli.service.base.BaseUnauthenticateService;
import com.crusterholz.meli.service.getCardIssues.model.GetCardIssuesRequest;
import com.crusterholz.meli.utils.ParamsUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class GetCardIssuesImpl extends BaseUnauthenticateService {
    public void get(GetCardIssuesRequest getCardIssuesRequest, Callback<List<CardIssue>> callback){
        GetCardIssuesInterface getCardIssuesService = retrofit.create(GetCardIssuesInterface.class);

        Map<String,String> params = ParamsUtil.getParamsMap();
        params.put("payment_method_id", getCardIssuesRequest.getPaymentMethodId());

        Call<List<CardIssue>> call = getCardIssuesService.getCardIssues(params);
        call.enqueue(callback);
    }
}
