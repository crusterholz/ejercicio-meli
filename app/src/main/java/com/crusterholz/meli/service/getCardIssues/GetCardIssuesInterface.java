package com.crusterholz.meli.service.getCardIssues;

import com.crusterholz.meli.model.CardIssue;
import com.crusterholz.meli.utils.ApiUrlUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GetCardIssuesInterface {
    @GET(ApiUrlUtils.API_GET_CARD_ISSUES)
    Call<List<CardIssue>> getCardIssues(@QueryMap Map<String,String> params);
}
