package com.crusterholz.meli.service.getCardIssues.model;

public class GetCardIssuesRequest {
    String paymentMethodId;

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }
}
