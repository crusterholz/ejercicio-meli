package com.crusterholz.meli.service.getInstallments;

import com.crusterholz.meli.model.Installment;
import com.crusterholz.meli.service.base.BaseUnauthenticateService;
import com.crusterholz.meli.service.getInstallments.model.GetInstallmentsRequest;
import com.crusterholz.meli.utils.ParamsUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class GetInstallmentsImpl extends BaseUnauthenticateService {
    public void get(GetInstallmentsRequest getInstallmentsRequest, Callback<List<Installment>> callback){
        GetInstallmentsInterface getInstallmentsService = retrofit.create(GetInstallmentsInterface.class);

        Map<String,String> params = ParamsUtil.getParamsMap();
        params.put("amount", getInstallmentsRequest.getAmount());
        params.put("payment_method_id", getInstallmentsRequest.getPaymentMethodId());
        params.put("issuer.id", getInstallmentsRequest.getIssuerId());

        Call<List<Installment>> call = getInstallmentsService.getInstallments(params);
        call.enqueue(callback);
    }
}
