package com.crusterholz.meli.service.getInstallments;

import com.crusterholz.meli.model.Installment;
import com.crusterholz.meli.utils.ApiUrlUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GetInstallmentsInterface {
    @GET(ApiUrlUtils.API_GET_INSTALLMENTS)
    Call<List<Installment>> getInstallments(@QueryMap Map<String,String> params);
}
