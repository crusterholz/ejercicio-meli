package com.crusterholz.meli.service.getPaymentMethods;

import com.crusterholz.meli.model.PaymentMethod;
import com.crusterholz.meli.service.base.BaseUnauthenticateService;
import com.crusterholz.meli.utils.ParamsUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class GetPaymentMethodsImpl extends BaseUnauthenticateService {
    public void get(Callback<List<PaymentMethod>> callback){
        GetPaymentMethodsInterface getPaymentMethodsService = retrofit.create(GetPaymentMethodsInterface.class);

        Map<String,String> params = ParamsUtil.getParamsMap();

        Call<List<PaymentMethod>> call = getPaymentMethodsService.getPaymentMethods(params);
        call.enqueue(callback);
    }
}
