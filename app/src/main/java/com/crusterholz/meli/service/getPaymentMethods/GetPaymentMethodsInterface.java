package com.crusterholz.meli.service.getPaymentMethods;

import com.crusterholz.meli.model.PaymentMethod;
import com.crusterholz.meli.utils.ApiUrlUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GetPaymentMethodsInterface {
    @GET(ApiUrlUtils.API_GET_PAYMENT_METHODS)
    Call<List<PaymentMethod>> getPaymentMethods(@QueryMap Map<String,String> params);
}

