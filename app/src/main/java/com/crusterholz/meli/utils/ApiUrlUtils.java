package com.crusterholz.meli.utils;

public class ApiUrlUtils {
    public static final String API_GET_PAYMENT_METHODS  = "v1/payment_methods";
    public static final String API_GET_CARD_ISSUES  = "v1/payment_methods/card_issuers";
    public static final String API_GET_INSTALLMENTS  = "v1/payment_methods/installments";
}
