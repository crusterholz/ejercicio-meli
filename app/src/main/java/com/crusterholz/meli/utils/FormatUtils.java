package com.crusterholz.meli.utils;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class FormatUtils {

    public static Locale localeAR = new Locale("es", "AR");
    public static Currency currency = Currency.getInstance("ARS");

    public static String formatAmount(String amount){
        double parsed = Double.parseDouble(amount);
        NumberFormat formatter = NumberFormat.getCurrencyInstance(localeAR);
        return formatter.format(parsed);
    }

    public static String getCurrencySymbol(){
        return currency.getSymbol(localeAR);
    }

    public static String getAmountWithoutSymbol(String amountString){
        String regex = "[" + getCurrencySymbol() + "]";
        return amountString.replaceAll(regex, "");
    }

    public static Boolean isValidAmountInput(String amountEditText){
        String amountString = getAmountWithoutSymbol(amountEditText);
        try {
            Double.parseDouble(amountString);
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }
}
