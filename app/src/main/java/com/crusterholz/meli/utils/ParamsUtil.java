package com.crusterholz.meli.utils;

import com.crusterholz.meli.BuildConfig;

import java.util.HashMap;
import java.util.Map;

public class ParamsUtil {
    public static Map<String,String> getParamsMap(){

        Map<String,String> params = new HashMap<>();
        params.put("public_key", BuildConfig.PUBLIC_KEY);

        return params;
    }
}
