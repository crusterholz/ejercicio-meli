package com.crusterholz.meli;

import com.crusterholz.meli.utils.FormatUtils;

import org.junit.Test;

import static org.junit.Assert.*;

public class AmountInputUnitTest {
    @Test
    public void validFormatWithWhiteSpaces() {
        assertEquals(true, FormatUtils.isValidAmountInput("  10.00  "));
    }

    @Test
    public void validFormatOneDecimal() {
        assertEquals(true, FormatUtils.isValidAmountInput("10.0"));
    }

    @Test
    public void validFormatTwoDecimal() {
        assertEquals(true, FormatUtils.isValidAmountInput("10.45"));
    }

    @Test
    public void validFormatThreeDecimal() {
        assertEquals(true, FormatUtils.isValidAmountInput("10.45"));
    }

    @Test
    public void validFormatWithoutDecimalAndPoint() {
        assertEquals(true, FormatUtils.isValidAmountInput("10."));
    }

    @Test
    public void validFormatWithoutDecimal() {
        assertEquals(true, FormatUtils.isValidAmountInput("10"));
    }

    @Test
    public void validFormatWithCorrectCurrencySymbol() {
        assertEquals(true, FormatUtils.isValidAmountInput(FormatUtils.getCurrencySymbol() + "10.00"));
    }

    @Test
    public void validFormatWithIncorrectCurrencySymbol() {
        assertEquals(false, FormatUtils.isValidAmountInput("U$S10.00"));
    }

    @Test
    public void invalidFormatLetter() {
        assertEquals(false, FormatUtils.isValidAmountInput("A"));
    }

    @Test
    public void invalidFormatEmpty() {
        assertEquals(false, FormatUtils.isValidAmountInput(""));
    }
}